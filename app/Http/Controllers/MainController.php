<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function tabel(){
        $mahasiswa = [ [
                'nama' => 'Hendro Dwi Prasetyo',
                'alamat' => 'Mojokerto',
                'umur' => '22'
            ],[
                'nama' => 'Hamim Nizar Y',
                'alamat' => 'Bojonegoro',
                'umur' => '22'
            ],[
                'nama' => 'Rosanita Firdausi Oktaviani',
                'alamat' => 'Jember',
                'umur' => '22'
            ]
        ];
        
        // dd($mahasiswa);
        return view('tabel', compact('mahasiswa'));
    }
}
