@extends('layout.main')

@section('title', 'Tabel')

@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
        <h1 class="mt-3">Data Mahasiswa</h1>
        <table class="table table-dark table-hover">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Alamat</th>
      <th scope="col">Umur</th>
    </tr>
  </thead>
  @foreach($mahasiswa as $data)
  <tbody>
    <tr>
      <th scope="row">{{ $loop->iteration }}</th>
      <td>{{ $data['nama'] }}</td>
      <td>{{ $data['alamat'] }}</td>
      <td>{{ $data['umur'] }}</td>
    </tr>
  </tbody>
  @endforeach
</table>
        </div>
    </div>
</div>

@endsection
